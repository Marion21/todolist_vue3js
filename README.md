# ToDoList_Vue3JS

Objectif du projet : 

Créer une interface qui permet de créer et supprimer des tâches
Vous devez sauvegarder la liste des tâches dans le localstorage du navigateur pour
pouvoir la récupérer au chargement de la page
On ne peut pas créer de tâches vides
Vos tâches doivent être uniques et doivent se présenter sous forme d'objet javascript
contenant les propriétés id et text . Vous devez générer des id uniques en
utilisant un timestamp : Date.now()

Outils :
Boostrap
